import numpy as np
from PIL import Image, ImageEnhance
from PIL import ImageStat
from pytesseract import *
import os
import cv2

def ocr(src):
    currentDir, _ = os.path.split(src)
    grayDir = os.path.join(currentDir, 'gray')
    if os.path.exists(grayDir) == False:
        os.mkdir(grayDir)
    _, fileNameExt = os.path.split(src)
    fileName, fileExt = os.path.splitext(fileNameExt)
    im = Image.open(src)
    #增加图片对比度，数值越高图片的对比度越高
    im = ImageEnhance.Contrast(im).enhance(2)
    #图片变成灰度图
    im = im.convert('L')
    #图片亮度统计
    stat = ImageStat.Stat(im)
    min, max = stat.extrema[0]
    #计算二值化的阈值，参数 0.9 越大则白色的部分越少，即图片里的杂乱信息越少
    threshold = (max - min) * 0.95 + min

    im.save(os.path.join(grayDir, fileName+"_gray"+fileExt))

    image = cv2.imread(os.path.join(grayDir, fileName + "_gray" + fileExt))
    image = cv2.medianBlur(image, 5)
    cv2.imwrite(os.path.join(grayDir, fileName + "_gray" + fileExt), image)

    im = Image.open(os.path.join(grayDir, fileName + "_gray" + fileExt))
    im = im.convert('L')
    #图片二值化
    binaryImage = im.point(initTable(threshold), '1')
    # binaryImage.show()
    hist = binaryImage.histogram()
    print(hist[0])
    print(hist[1])
    print(hist[1]/hist[0])
    binaryImage.save(os.path.join(grayDir, fileName+"_gray"+fileExt))

    if hist[1]/hist[0] > 0.3:
        image = cv2.imread(os.path.join(grayDir, fileName+"_gray"+fileExt))
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        #图片腐蚀处理
        image = cv2.erode(image, kernel)
        # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        # image = cv2.dilate(image, kernel)
        cv2.imwrite(os.path.join(grayDir, fileName+"_gray"+fileExt), image)

    im = Image.open(os.path.join(grayDir, fileName + "_gray" + fileExt))
    #图片识别
    result = image_to_string(im, config='-psm 7 letter')
    resultList = result.split(' ')
    result = ''.join(resultList)
    print(result)
    return result

def initTable(threshold=128):
    table = []
    for i in range(256):
        if i < threshold:
            table.append(0)
        else:
            table.append(1)
    return table

if __name__ == '__main__':
    ocr("/Users/kassol/Desktop/tttt.png")