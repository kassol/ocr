#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask, request, jsonify
import os
import base64
import uuid

from testocr import ocr

app = Flask(__name__)

@app.route('/vin', methods=['POST'])
def vin():
    data = request.form

    ext = data.get('ext', 'jpg')
    img = data.get('img')
    if img is None or ext is None:
        return jsonify({"code":0, "msg":"参数错误", "date":""})
    curDir = os.path.abspath('.')
    uploadDir = os.path.join(curDir, 'upload')
    if os.path.exists(uploadDir) == False:
        os.mkdir(uploadDir)
    imgPath = os.path.join(uploadDir, str(uuid.uuid1())+'.'+ext)
    app.logger.debug(imgPath)
    print(imgPath)
    with open(imgPath, 'wb') as fh:
        fh.write(base64.b64decode(img))
    result = ocr(imgPath)
    app.logger.debug(result)
    print(result)
    if result == "":
        return jsonify({"code": 0, "msg": "识别失败", "data": ""})
    else:
        return jsonify({"code": 1, "msg": "成功", "data": result})

# Run
if __name__ == '__main__':
    app.run(
        host = "0.0.0.0",
        port = 5000
    )


