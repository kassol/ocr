#!/usr/bin/env python3
# -*- coding: utf-8 -*-

' a ocr module '

__author__ = 'Kassol'

from PIL import Image
from pytesseract import *
import cv2
import os
import numpy as np

# from matplotlib import pyplot as plt
# import matplotlib

# matplotlib.use('Agg')

def ocr(src):
    image = cv2.imread(src)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    currentDir, _ = os.path.split(src)
    grayDir = os.path.join(currentDir, 'gray')
    if os.path.exists(grayDir) == False:
        os.mkdir(grayDir)
    _, fileNameExt = os.path.split(src)
    fileName, fileExt = os.path.splitext(fileNameExt)
    cv2.imwrite(os.path.join(grayDir, fileName + "_hsv" + fileExt), hsv)
    im = Image.open(os.path.join(grayDir, fileName + "_hsv" + fileExt))
    h, s, v = im.split()
    h.save(os.path.join(grayDir, fileName + "_h" + fileExt))
    s.save(os.path.join(grayDir, fileName + "_s" + fileExt))
    s.save(os.path.join(grayDir, fileName + "_v" + fileExt))
    # plt.subplot(131), plt.imshow(gray, "gray")
    # plt.title("source image"), plt.xticks([]), plt.yticks([])
    # plt.subplot(132), plt.hist(gray.ravel(), 256)
    # plt.title("Histogram"), plt.xticks([]), plt.yticks([])
    ret1, th1 = cv2.threshold(gray, 0, 255, cv2.THRESH_OTSU)  # 方法选择为THRESH_OTSU
    # 腐蚀
    # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    # th1 = cv2.erode(th1, kernel)
    # 膨胀
    # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    # th1 = cv2.dilate(th1, kernel)

    # plt.subplot(133), plt.imshow(th1, "gray")
    # plt.title("OTSU,threshold is " + str(ret1)), plt.xticks([]), plt.yticks([])
    # plt.show()
    cv2.imwrite(os.path.join(grayDir, fileName+"_gray"+fileExt), th1)

    im = Image.open(os.path.join(grayDir, fileName+"_gray"+fileExt))
    # im = im.convert('L')
    # im.show()
    # binaryImage = im.point(initTable(), '1')
    # binaryImage.show()
    result = image_to_string(im, config='tessedit_char_whitelist=0123456789ABCDEFGHJKLMNPRSTUVWXYZ -psm 6')
    return result


def getGray(img):
    numGray = [0 for i in range(pow(2, img.depth))]
    for h in range(img.height):
        for w in range(img.width):
            numGray[int(img[h, w])] += 1
    return numGray


def getThres(gray):
    maxV = 0
    bestTh = 0
    w = [0 for i in range(len(gray))]
    px = [0 for i in range(len(gray))]
    w[0] = gray[0]
    px[0] = 0
    for m in range(1, len(gray)):
        w[m] = w[m - 1] + gray[m]
        px[m] = px[m - 1] + gray[m] * m
    for th in range(len(gray)):
        w1 = w[th]
        w2 = w[len(gray) - 1] - w1
        if (w1 * w2 == 0):
            continue
        u1 = px[th] / w1
        u2 = (px[len(gray) - 1] - px[th]) / w2
        v = w1 * w2 * (u1 - u2) * (u1 - u2)
        if v > maxV:
            maxV = v
            bestTh = th
    return bestTh

def initTable(threshold=128):
    table = []
    for i in range(256):
        if i < threshold:
            table.append(0)
        else:
            table.append(1)
    return table



if __name__ == '__main__':
    ocr("/Users/kassol/Desktop/1.png")